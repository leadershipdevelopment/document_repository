﻿/*!
 * SEI MDB Master JavaScript Library
 *
 * Copyright 2013 SEI
 * All Rights Reserved
 *
 */

//Global Variables



    // Left Navigation Toggle Function
    function toggleLeftNavigation() {
        // Toggle Active Class
        $(".menuLeft").toggleClass("active");

        // Toggle Arrow
        $(".fa .fa-chevron-left").addClass("fa-flip-horizontal");
    }

    //Close Toggle
    $(".menu_left_head").click(function() {
        $(".menuLeft .active").removeClass("active");
    });

    // Resize Events
    $(document).resize(function () { resizeElements(); });
    $(window).resize(function () { resizeElements(); });

    // Toggle Left Nav
    $(".menu_left_head").click(function () { toggleLeftNavigation(); });

    $("body").click(function (e) {
        // Target
        var target = $(e.target);

        // Default to false
        var isLeftMenu = false;

        // Find if the item or parents have menuLeft class
        target.parentsUntil("body").each(function () {
            if ($(this).hasClass("menuLeft")) {
                isLeftMenu = true;
            }
        });

        // If not left menu and menuLeft is active
        if (!isLeftMenu && $(".menuLeft.active").val() !== undefined) {
            toggleLeftNavigation();
        }
    });

   