﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Document_Repository.Models;

namespace Document_Repository.Controllers
{
    public class DocumentController : Controller
    {
        private List<DocumentViewModel> _DocumentListing;

        public DocumentController()
        {
            _DocumentListing = new List<DocumentViewModel>();

            _DocumentListing.Add(new DocumentViewModel()
            {
                FileType = "Hello"
                ,
                FileName = "ThisDocumentName"
                ,
                FileDescription = "Good Stuff"
                ,
                DateModified = DateTime.Parse("2/4/2015")
                ,
                AuthorName = "John A. Hendrix"
                ,
                FileSize = "3 MB"
                ,
                FileAction = "Copy Link | Download"

            });
        }

       
        // GET: Document
           public ActionResult Index(){ 
            
           return View(_DocumentListing.ToArray());
        }
    }
}