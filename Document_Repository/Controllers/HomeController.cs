﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Document_Repository.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.AlfrescoApiUrl = WebConfigurationManager.AppSettings["URL-Alfresco-API"];
            ViewBag.DashboardUrl = WebConfigurationManager.AppSettings["URL-Dashboard"];
            ViewBag.DocumentRepoUrl = WebConfigurationManager.AppSettings["URL-Document-Repo"];
            ViewBag.TaskUiUrl = WebConfigurationManager.AppSettings["URL-Task-UI"];
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}