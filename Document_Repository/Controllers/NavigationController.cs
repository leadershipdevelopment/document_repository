﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Document_Repository.Controllers
{
    public class NavigationController : Controller
    {
        //public PartialViewResult MyNotifications()
        //{
        //    Models.LeftNavigationViewModel myNotificationsDoc =
        //        new Models.LeftNavigationViewModel();

        //    myNotificationsDoc.Title = "Notifications";

        //    // creating a placeholder for my recent documents
        //    List<Models.DocumentViewModel> myNotifications =
        //        new List<Models.DocumentViewModel>();

        //    myNotifications.Add(
        //        new Models.DocumentViewModel()
        //        {
        //            FileName = "NotificationsReport.txt"
        //        });

        //    // adding documents to my navigation model
        //    myNotificationsDoc.Documents = myNotifications.ToArray();

        //    return PartialView("~/Views/Navigation/_leftNavListing.cshtml", myNotifications);
        //}

        public PartialViewResult MyRecentDocs()
        {
            Models.LeftNavigationViewModel myRecentDocsLeftNav =
                new Models.LeftNavigationViewModel();

            myRecentDocsLeftNav.Title = "Recent Documents";

            // creating a placeholder for my recent documents
            List<Models.DocumentViewModel> myRecentDocuments =
                new List<Models.DocumentViewModel>();

            myRecentDocuments.Add(
                new Models.DocumentViewModel()
                {
                    FileName = "RecentReport.txt"
                });

            // adding documents to my navigation model
            myRecentDocsLeftNav.Documents = myRecentDocuments.ToArray();

            return PartialView("~/Views/Navigation/_leftNavListing.cshtml", myRecentDocsLeftNav);
        }



        public PartialViewResult MyDocuments()
        {
            Models.LeftNavigationViewModel myDocsLeftNav = 
                new Models.LeftNavigationViewModel();
            
            myDocsLeftNav.Title = "My Documents";

            // creating a placeholder for my recent documents
            List<Models.DocumentViewModel> myRecentDocuments = 
                new List<Models.DocumentViewModel>();
            
            myRecentDocuments.Add(
                new Models.DocumentViewModel()
                { 
                    FileName = "Report.txt"
            });

            // adding documents to my navigation model
            myDocsLeftNav.Documents = myRecentDocuments.ToArray();

            return PartialView("~/Views/Navigation/_leftNavListing.cshtml", myDocsLeftNav);
        }

        public PartialViewResult SharedDocuments()
        {
            Models.LeftNavigationViewModel sharedDocsLeftNav =
                new Models.LeftNavigationViewModel();

            sharedDocsLeftNav.Title = "Shared Documents";

            // creating a placeholder for my recent documents
            List<Models.DocumentViewModel> sharedDocuments =
                new List<Models.DocumentViewModel>();

            sharedDocuments.Add(
                new Models.DocumentViewModel()
                {
                    FileName = "DocumentNameHere.txt"
                });

            sharedDocuments.Add(
                new Models.DocumentViewModel()
                {
                    FileName = "ImportantReport.pdf"
                });

            // adding documents to my navigation model
            sharedDocsLeftNav.Documents = sharedDocuments.ToArray();

            return PartialView("~/Views/Navigation/_leftNavListing.cshtml", sharedDocsLeftNav);
        }

        public PartialViewResult MyRepository()
        {
            Models.LeftNavigationViewModel myRepoLeftNav =
                new Models.LeftNavigationViewModel();

            myRepoLeftNav.Title = "My Repository";

            // creating a placeholder for my recent documents
            List<Models.DocumentViewModel> myRecentDocuments =
                new List<Models.DocumentViewModel>();

            myRecentDocuments.Add(
                new Models.DocumentViewModel()
                {
                    FileName = "RepositoryDoc1.txt"
                });

            // adding documents to my navigation model
            myRepoLeftNav.Documents = myRecentDocuments.ToArray();

            return PartialView("~/Views/Navigation/_leftNavListing.cshtml", myRepoLeftNav);
        }
    }
}