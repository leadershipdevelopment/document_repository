﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Document_Repository.Startup))]
namespace Document_Repository
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
