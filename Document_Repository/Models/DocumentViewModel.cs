﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



namespace Document_Repository.Models
{
    public class DocumentViewModel {
        public String FileType { get; set; }
        public String FileName { get; set; }
        public String FileDescription { get; set; }
        public DateTime DateModified { get; set; }
        public String AuthorName { get; set; }
        public String FileSize { get; set; }
        public String FileAction { get; set; }
        public List<DocumentViewModel> _DocumentListing {get; set;}

    }}



