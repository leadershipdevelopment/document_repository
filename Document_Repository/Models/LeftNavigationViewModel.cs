﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Document_Repository.Models
{
    public class LeftNavigationViewModel
    {
        private String _ID;

        public LeftNavigationViewModel()
        {
            _ID = Guid.NewGuid().ToString();
        }

        public String ID { get { return _ID; } }
        public String Title { get; set; }

        public Boolean DocumentsExist { 
            get {
                
                if (this.Documents.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        } }       
        
        public Models.DocumentViewModel[] Documents { get; set; }
    }
}

